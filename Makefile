#go#
tidy:
	go mod tidy

run:
	go run cmd/server/main.go

build:
	cd cmd/server; go build -o ../../bin/go-sso

exec:
	./bin/go-sso

startapp: build exec

#docker#
bash:
	docker exec -it mysql-go-sso bash

ipmysql:
	docker exec -i mysql-go-sso hostname -i | awk '{print $1}'

dbuild:
	docker build . -t go-sso

dprune:
	docker image prune -f

dcpdb:
	docker cp db_dump/mysql/sso_09072021.sql mysql-go-sso:.                                                                                            ─╯

stop_sso:
	docker container stop go-sso

start_mysql:
	docker container start mysql-go-sso

ctl: stop_sso start_mysql ipmysql

pqctl: stop_sso pqip

#git#
gpod:
	git push origin develop

#docker-compose#
dcup:
	docker-compose up -d app

dcdown:
	docker-compose down app

dcstart:
	docker-compose start app

dcstop:
	docker-compose stop app

dclogs:
	docker-compose logs -f app
	
#rebuild and redeploy#
re: dcstop dbuild dprune dcup dclogs

restart: dcdown dcup

#curl
curl_login:
	curl --request POST --url http://localhost:9090/api/login --header 'Content-Type: application/json' --cookie session_token=3c5452cc-4567-4ea5-8b8c-c1a85f9234be --data '{ "username": "201810225010", "password": "alifudin122" }'

curl_welcome:
	curl --request GET --url http://localhost:9090/api/welcome --cookie session_token=3c5452cc-4567-4ea5-8b8c-c1a85f9234be

curl_logout:
	curl --request GET --url http://localhost:9090/api/logout

pqbash:
	docker exec -it pq-arsip-surat-unggulan hostname bash
	
pqip:
	docker exec -it pq-arsip-surat-unggulan hostname -i | awk '{print $1}'

gpom:
	git push origin main

gphm:
	git push heroku main

gcam:
	git commit -am "build"

#heroku
hlogs:
	heroku logs --tail

hdeploy: build gcam gpom gphm hlogs