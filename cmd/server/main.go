package main

import (
	"github.com/joho/godotenv"

	database "gitlab.com/alifudin-a/go-sso/pkg/database/postgresql"
	rds "gitlab.com/alifudin-a/go-sso/pkg/database/redis"
	"gitlab.com/alifudin-a/go-sso/pkg/server"
)

func init() {
	_ = godotenv.Load(".env")
}

func main() {
	// database.OpenMySQL()
	database.OpenDBPQ()
	rds.OpenRedis()
	server.InitRoute()
}
