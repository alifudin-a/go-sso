package server

import (
	"net/http"

	"github.com/go-playground/validator/v10"
	_ "github.com/heroku/x/hmetrics/onload"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/alifudin-a/go-sso/pkg/domain/helper"
	handler "gitlab.com/alifudin-a/go-sso/pkg/http/rest/sso"
)

// InitRoute : initialize echo router
func InitRoute() *echo.Echo {
	e := echo.New()

	port := "9090"

	// port := os.Getenv("PORT")
	// if port == "" {
	// 	log.Println("$PORT must be set!")
	// }

	e.Validator = &helper.CustomValidator{Validator: validator.New()}
	e.HTTPErrorHandler = helper.CustomReadableError

	e.Use(middleware.Recover())
	e.Use(middleware.Gzip())
	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "==> METHOD=${method}, HOST=${host}, URI=${uri}, STATUS=${status}, " +
			"LATENCY_HUMAN=${latency_human}, ERROR=${error}\n",
	}))
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{
			echo.HeaderOrigin,
			echo.HeaderSetCookie,
			echo.HeaderAccessControlAllowCredentials,
			echo.HeaderContentType,
			echo.HeaderAccept,
			echo.HeaderAuthorization,
			echo.HeaderContentLength,
			echo.HeaderAcceptEncoding,
			echo.HeaderAccessControlAllowOrigin,
			echo.HeaderAccessControlAllowHeaders,
			echo.HeaderContentDisposition,
		}, //"app-key", "user-token"},
		ExposeHeaders: []string{
			echo.HeaderOrigin,
			echo.HeaderSetCookie,
			echo.HeaderContentType,
			echo.HeaderAccept,
			echo.HeaderAuthorization,
			echo.HeaderContentLength,
			echo.HeaderAcceptEncoding,
			echo.HeaderAccessControlAllowOrigin,
			echo.HeaderAccessControlAllowHeaders,
			echo.HeaderAccessControlAllowCredentials,
			echo.HeaderContentDisposition,
		}, //"app-key", "user-token"},
		AllowMethods: []string{
			http.MethodGet,
			http.MethodHead,
			http.MethodPut,
			http.MethodPatch,
			http.MethodPost,
			http.MethodDelete,
			http.MethodOptions,
		},
		AllowCredentials: true,
	}))

	e.Static("/", "./static")

	api := e.Group("/api")

	// api.GET("/welcome", handler.NewWelcome().WelcomeHandler)
	api.POST("/auth", handler.NewAuth().AuthHandler)
	api.POST("/v2/auth", handler.NewAuthV2Handler().AuthV2Handler)
	api.GET("/v2/auth", handler.NewAuthJWTHandler().AuthJWTHandler)

	//Logout
	api.POST("/logout", handler.NewLogoutHandler().LogoutHandler)

	e.Logger.Fatal(e.Start(":" + port))

	return e
}
