package database

import (
	"context"
	"log"

	"github.com/go-redis/redis/v8"
)

var RDC *redis.Client
var ctx = context.Background()

func OpenRedis() {

	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       6,
	})

	pong, err := client.Ping(ctx).Result()
	if err != nil {
		log.Panicln("An error occured while sending ping to redis => [", err, "]")
	}

	log.Println("Redis connection =====> PING ->", pong)

	RDC = client
}
