package database

import (
	"fmt"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// DB : global variable db connection
var DB *sqlx.DB

// OpenMySQL : open connection for mysql
func OpenMySQL() {

	var (
		host     = os.Getenv("DB_HOST")
		port     = os.Getenv("DB_PORT")
		user     = os.Getenv("DB_USER")
		password = os.Getenv("DB_PASS")
		dbname   = os.Getenv("DB_NAME")
	)

	mysqlInfo := fmt.Sprintf("%s:%s@(%s:%s)/%s", user, password, host, port, dbname)

	db, err := sqlx.Open("mysql", mysqlInfo)
	if err != nil {
		log.Println("An error occured while connection to database: ", err)
	} else {
		log.Printf("Connected to database %s!", dbname)
	}

	db.SetConnMaxLifetime(time.Minute * 5)
	db.SetMaxOpenConns(10)
	db.SetMaxIdleConns(10)

	DB = db
}
