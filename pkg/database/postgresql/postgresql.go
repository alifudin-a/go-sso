package database

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var DBPQ *sqlx.DB

func OpenDBPQ() {
	var (
		host     = os.Getenv("DB_HOST")
		port     = os.Getenv("DB_PORT")
		user     = os.Getenv("DB_USER")
		password = os.Getenv("DB_PASS")
		dbname   = os.Getenv("DB_NAME")
	)

	// psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
	// 	"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	psqlInfo := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable", user, password, host, port, dbname)

	// Initialise a new connection pool
	db, err := sqlx.Open("postgres", psqlInfo)
	if err = db.Ping(); err != nil {
		log.Println("An error occured while connecting to database: ", err)
	} else {
		log.Printf("Connected to database %s\n", dbname)
	}

	db.DB.SetMaxIdleConns(10)
	db.DB.SetMaxOpenConns(10)
	db.DB.SetConnMaxLifetime(time.Minute * 5)

	DBPQ = db

}
