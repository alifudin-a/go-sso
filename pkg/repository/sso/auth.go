package repository

import (
	"encoding/json"
	"log"

	"github.com/jmoiron/sqlx/types"
	database "gitlab.com/alifudin-a/go-sso/pkg/database/postgresql"
	models "gitlab.com/alifudin-a/go-sso/pkg/domain/models/sso"
	query "gitlab.com/alifudin-a/go-sso/pkg/domain/query/sso"
)

type AuthRepository interface {
	Auth(arg AuthParams) (*models.Auth, error)
}

type repository struct{}

func NewAuthRepository() AuthRepository {
	return &repository{}
}

type AuthParams struct {
	Username string
}

func (*repository) Auth(arg AuthParams) (*models.Auth, error) {

	var auth models.Auth
	var db = database.DBPQ
	var jsonString types.JSONText

	row, err := db.Queryx(query.LoginV3PQ, arg.Username)
	if err != nil {
		log.Println("Error on db.Queryx: ", err)
		return nil, err
	}

	for row.Next() {
		err = row.Scan(&jsonString)
		if err != nil {
			log.Println("Error on row.Scan: ", err)
			return nil, err
		}

		err = json.Unmarshal([]byte(jsonString), &auth)
		if err != nil {
			log.Println("Error on json.Unmarshal: ", err)
			return nil, err
		}
	}

	return &auth, nil
}
