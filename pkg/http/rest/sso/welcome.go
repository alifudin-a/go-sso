package rest

// import (
// 	"net/http"

// 	"github.com/labstack/echo/v4"
// 	database "gitlab.com/alifudin-a/go-sso/pkg/database/redis"
// 	response "gitlab.com/alifudin-a/go-sso/pkg/http/response"
// 	"golang.org/x/net/context"
// )

// type welcome struct{}

// func NewWelcome() *welcome {
// 	return &welcome{}
// }

// func (w *welcome) WelcomeHandler(c echo.Context) (err error) {

// 	var resp response.Response
// 	var rd = database.RC
// 	var ctx = context.Background()

// 	cookie, err := c.Cookie("session_token")
// 	if err != nil {
// 		if err == http.ErrNoCookie {
// 			resp.Code = http.StatusUnauthorized
// 			resp.Message = "Unauthorized! There is no cookie!"
// 			return c.JSON(http.StatusUnauthorized, resp)
// 		}

// 		resp.Code = http.StatusBadRequest
// 		resp.Message = "Invalid request!"
// 		return c.JSON(http.StatusBadRequest, resp)
// 	}

// 	sessionToken := cookie.Value

// 	getToken := rd.Get(ctx, sessionToken)
// 	if getToken == nil {
// 		resp.Code = http.StatusUnauthorized
// 		resp.Message = "Unauthorized! There is no session token!"
// 		return c.JSON(http.StatusUnauthorized, resp)
// 	}

// 	resp.Code = http.StatusOK
// 	resp.Message = "Welcome!"
// 	resp.Data = map[string]interface{}{
// 		"token": getToken.Val(),
// 	}

// 	return c.JSON(http.StatusOK, resp)
// }
