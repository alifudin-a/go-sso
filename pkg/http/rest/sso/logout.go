package rest

import (
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	response "gitlab.com/alifudin-a/go-sso/pkg/http/response"
)

type logout struct{}

func NewLogoutHandler() *logout {
	return &logout{}
}

func (*logout) LogoutHandler(c echo.Context) (err error) {

	var resp response.Response

	cookie1 := new(http.Cookie)
	cookie1.Name = "session_token"
	cookie1.Value = ""
	cookie1.Expires = time.Now().Add(-time.Hour)
	cookie1.Path = "/"

	cookie2 := new(http.Cookie)
	cookie2.Name = "tbl_sessions"
	cookie2.Value = ""
	cookie2.Expires = time.Now().Add(-time.Hour)
	cookie2.HttpOnly = true
	cookie2.Path = "/"

	cookie3 := new(http.Cookie)
	cookie3.Name = "ci_session"
	cookie3.Value = ""
	cookie3.Expires = time.Now().Add(-time.Hour)
	cookie3.HttpOnly = true
	cookie3.Path = "/"

	cookie4 := new(http.Cookie)
	cookie4.Name = "ubj_last_url"
	cookie4.Value = ""
	cookie4.Expires = time.Now().Add(-time.Hour)
	cookie4.Path = "/"

	c.SetCookie(cookie1)
	c.SetCookie(cookie2)
	c.SetCookie(cookie3)
	c.SetCookie(cookie4)

	resp.Code = http.StatusOK
	resp.Message = "Logout Berhasil!"

	return c.JSON(http.StatusOK, resp)
}
