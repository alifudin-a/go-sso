package rest

import (
	"net/http"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/alifudin-a/go-sso/pkg/domain/builder"
	models "gitlab.com/alifudin-a/go-sso/pkg/domain/models/sso"
	response "gitlab.com/alifudin-a/go-sso/pkg/http/response"
	repository "gitlab.com/alifudin-a/go-sso/pkg/repository/sso"
	"golang.org/x/crypto/bcrypt"
)

type Auth struct{}

func NewAuth() *Auth {
	return &Auth{}
}

type AuthTokenClaims struct {
	*jwt.StandardClaims
	models.Auth
}

func (a *Auth) AuthHandler(c echo.Context) (err error) {
	var resp response.Response
	var req = new(models.Auth)
	var auth *models.Auth

	err = a.validate(req, c)
	if err != nil {
		resp.Code = http.StatusInternalServerError
		resp.Message = "Login Gagal! Username dan Password tidak boleh kosong!"
		return c.JSON(http.StatusInternalServerError, resp)
	}

	repo := repository.NewAuthRepository()

	arg := builder.Auth(req)

	auth, err = repo.Auth(arg)
	if err != nil {
		resp.Code = http.StatusUnauthorized
		resp.Message = "Login Gagal! Periksa kembali Username dan Password Anda!"
		return c.JSON(http.StatusUnauthorized, resp)
	} else if arg.Username == "" {
		resp.Code = http.StatusUnauthorized
		resp.Message = "Login Gagal! Periksa kembali Username dan Password Anda!"
		return c.JSON(http.StatusUnauthorized, resp)
	} else if arg.Username != auth.Username {
		resp.Code = http.StatusUnauthorized
		resp.Message = "Login Gagal! Periksa kembali Username dan Password Anda!"
		return c.JSON(http.StatusUnauthorized, resp)
	}

	err = bcrypt.CompareHashAndPassword([]byte(auth.Password), []byte(req.Password))
	if err != nil {
		resp.Code = http.StatusUnauthorized
		resp.Message = "Login Gagal! Periksa kembali Username dan Password Anda!"
		return c.JSON(http.StatusUnauthorized, resp)
	}

	var lisApps []models.Apps
	for _, apps := range auth.Apps {
		lisApps = append(lisApps, apps)
	}

	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims = &AuthTokenClaims{
		&jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 5).Unix(),
		},
		models.Auth{
			User: models.User{
				ID:        auth.ID,
				Username:  auth.Username,
				Fullname:  auth.Fullname,
				Email:     auth.Email,
				GroupID:   auth.GroupID,
				GroupName: auth.GroupName,
			},
			Apps: lisApps,
		},
	}

	stringJwt, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return err
	}

	cookie := new(http.Cookie)
	cookie.Name = "session_token"
	cookie.Value = stringJwt
	cookie.Expires = time.Now().Add(1 * time.Hour)
	cookie.Path = "/"
	// cookie.Domain = "172.168.1.41"
	cookie.Domain = "localhost"
	// cookie.HttpOnly = true
	// cookie.Secure = true
	// cookie.SameSite = 4
	c.SetCookie(cookie)

	resp.Code = http.StatusOK
	resp.Message = "Login Berhasil!"

	return c.JSON(http.StatusOK, resp)
}

func (*Auth) validate(req *models.Auth, c echo.Context) (err error) {
	if err = c.Bind(req); err != nil {
		return
	}

	return c.Validate(req)
}
