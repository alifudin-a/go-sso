package rest

import (
	"context"
	"net/http"

	"github.com/labstack/echo/v4"
	database "gitlab.com/alifudin-a/go-sso/pkg/database/redis"
	response "gitlab.com/alifudin-a/go-sso/pkg/http/response"
)

type authjwt struct{}

func NewAuthJWTHandler() *authjwt {
	return &authjwt{}
}

func (*authjwt) AuthJWTHandler(c echo.Context) (err error) {
	var resp response.Response
	var rdc = database.RDC
	var ctx = context.Background()

	key := c.QueryParam("key")

	jwt, err := rdc.Get(ctx, key).Result()
	if err != nil {
		resp.Code = http.StatusBadRequest
		resp.Message = "Key not found!"
		return c.JSON(http.StatusBadRequest, resp)
	}

	resp.Code = http.StatusOK
	resp.Message = "Success!"
	resp.Data = map[string]interface{}{
		"key": jwt,
	}

	return c.JSON(http.StatusOK, resp)
}
