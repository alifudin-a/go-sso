package builder

import (
	models "gitlab.com/alifudin-a/go-sso/pkg/domain/models/sso"
	repository "gitlab.com/alifudin-a/go-sso/pkg/repository/sso"
)

func Auth(params *models.Auth) repository.AuthParams {
	var resp repository.AuthParams

	resp.Username = params.Username

	return resp
}
