package models

// User : models struct user for login
type User struct {
	ID        int    `json:"id,omitempty" db:"id"`
	Username  string `json:"username" validate:"required" db:"username"`
	Password  string `json:"password,omitempty" validate:"required" db:"password"`
	Fullname  string `json:"full_name" db:"full_name"`
	Email     string `json:"email" db:"email"`
	GroupID   int    `json:"group_id,omitempty" db:"group_id"`
	GroupName string `json:"group_name" db:"group_name"`
}

type Apps struct {
	AppID          int    `json:"app_id,omitempty" db:"app_id"`
	AppName        string `json:"app_name" db:"app_name"`
	AppDescription string `json:"app_description" db:"app_description"`
	AppImage       string `json:"app_image" db:"app_image"`
	AppUrl         string `json:"app_url" db:"app_url"`
}

type Auth struct {
	User
	Apps []Apps `json:"apps"`
}
