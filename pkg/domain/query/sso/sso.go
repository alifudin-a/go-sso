package query

// // GetUser : select all from table users by id
// var GetUser = `SELECT * FROM users WHERE id = ?;`

// // Login : query for checking username and password
// var Login = "SELECT id, `group`, username, fullname, email, created_at, updated_at FROM users WHERE username = ? AND password = ?;"

// // Login : query for checking username and password
// var LoginV2 = "SELECT id, `group`, username, password, fullname, email, created_at, updated_at FROM users WHERE username = ?;"

var Login = "select " +
	"u.id , u.username , u.password , u.full_name , u.email," +
	"g.id as 'group_id', g.name as 'group_name', a.id as 'app_id'," +
	"a.name as 'app_name', a.description as 'app_description'" +
	"from users u " +
	"join users_groups ug on u.id = ug.user_id " +
	"join `groups` g on ug.group_id = g.id " +
	"join application_groups ag on g.id = ag.group_id " +
	"join application a on ag.application_id = a.id " +
	"where u.username = ? order by a.id asc;"

var LoginV3 = `
SELECT JSON_OBJECT(
	"id", tu.id,
	"username", tu.username,
	"password", tu.password,
	"full_name", tu.full_name,
	"email", tu.email,
	"group_id", tg.id,
	"group_name", tg.name,
	"apps", (
		SELECT JSON_ARRAYAGG(JSON_OBJECT(
			"app_id", ta2.id,
			"app_name", ta2.name,
			"app_description", ta2.description,
			"app_image", ta2.image,
			"app_url", ta2.url
		))
		FROM tbl_application ta2 
		JOIN tbl_application_groups tag2 ON ta2.id = tag2.application_id 
		WHERE tag2.group_id = 7
	)
)
FROM tbl_users tu 
JOIN tbl_users_groups tug ON tu.id = tug.user_id 
JOIN tbl_groups tg ON tug.group_id = tg.id
JOIN tbl_application_groups tag ON tg.id = tag.group_id 
JOIN tbl_application ta ON tag.application_id = ta.id 
WHERE tu.username = ? GROUP BY tu.id;
`
var LoginV3PQ = `
SELECT ROW_TO_JSON(t)
FROM (
	SELECT 
		tu.username,
		tu.password,
		tu.full_name,
		tu.email,
		tg."name" AS "group_name",
		(
			SELECT ARRAY_TO_JSON(ARRAY_AGG(ROW_TO_JSON(d)))
			FROM (
				SELECT
					ta.name AS "app_name",
					ta.description AS "app_description",
					ta.image AS "app_image",
					ta.url AS "app_url"
				FROM tbl_applications ta
				JOIN tbl_applications_groups tag ON ta.id = tag.application_id 
				WHERE tag.group_id = tug.group_id
			)d
		) AS apps
	FROM tbl_users tu 
	JOIN tbl_users_groups tug ON tu.id = tug.user_id
	JOIN tbl_groups tg ON tug.group_id = tg.id 
	JOIN tbl_applications_groups tag2 ON tg.id = tag2.group_id 
	JOIN tbl_applications ta2 ON tag2.application_id = ta2.id 
	WHERE tu.username = $1 ORDER BY tu.id limit 1
)t;
`
